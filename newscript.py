#!/bin/bash

import os, subprocess

def runGitleaks():
        # fetch the last and second last commit ids.
        #lc= subprocess.Popen(['git log --format="%H" -n 1'], stdout=subprocess.PIPE, shell=True)
        #(lcc , err) = lc.communicate()
        lc=os.popen('git log --format="%H" -n 1').read().strip()
        slc= os.popen('git log -n 1 --skip 1 --pretty=format:"%H"').read()
        cmd = "gitleaks detect --source .  --redact --no-banner --log-opts='{0}...{1}' --report-format json --report-path csi_report.json".format(lc,slc)
        os.system(cmd)
        
        


runGitleaks()
